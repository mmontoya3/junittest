M5 - Entorns de Desenvolupament.
Per: Marta Montoya Anton.

# PRÀCTICA JUNIT TEST - JOC DE PROVES

#### Gestió

#### Covertura del projecte

>Com es pot apreciar a la captura de pantalla, la covertura actual del meu projecte és del 38,2%.

![](https://i.imgur.com/2juFBcd.png)


###### Propostes per millorar la covertura:
>Buscar possibles errors de codi, gràcies a la covertura m'he adonat que tenia errors en el codi... (M'havia oblida tancar les claus correctament d'uns condicionals). la covertura ha pujat a un 38,7%.

![](https://i.imgur.com/7DARL8v.png)


### Els test

![](https://i.imgur.com/rGioB7k.png)



##### Test 1: Treballadors amb un sou superior a 2000euros (assertEquals).

![](https://i.imgur.com/gPpgHks.png)

##### Test 2: Treballadors amb una edat compresa entre 60 i 65 anys ambdós incloses (assertEquals).

![](https://i.imgur.com/qJioDdG.png)

##### Test 3: Existeix un determinat treballador (assertTrue).

![](https://i.imgur.com/9O5ngUG.png)

##### Test 4: Comparador de sous tipus souMesGran (Treballador treballador) amb assertEquals.

![](https://i.imgur.com/ucjvXeY.png)

#### Resultats: 

![](https://i.imgur.com/jeGcRd8.png)


### JavaDoc

>A continuació mostraré unes poques captures del JavaDoc que he generat mitjançan comentaris que he fet al projecte des de Eclipse.
>Una vista de la pàgina principal:

![](https://i.imgur.com/BC0n5rS.png)

#### Classe Empresa

>Aquesta classe modela una empresa amb múltiples treballadors (Relació de 1 a N).

![](https://i.imgur.com/rPFcPTU.png)

#### Classe Treballador

>Aquesta classe representa a un treballador de una empresa (Relació 1 a N).

![](https://i.imgur.com/9uos9lr.png)
